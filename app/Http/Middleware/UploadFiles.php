<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\UploadedFile;

class UploadFiles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->isMethod('post') || $request->isMethod('put')) {
            $attributes = $this->upload($request->all());

            foreach ($attributes as $key => $value) {
                $request->request->set($key, $value);
            }
        }

        return $next($request);
    }

    private function upload($array)
    {
        $attributes = array();

        foreach ($array as $key => $value) {
            if (is_array($file = $value)) $file = $this->upload($file);

            if ($file instanceof UploadedFile) {
                $path = 'storage/' . $file->storeAs('', $filename = $file->getClientOriginalName());

                $file = asset($path);
            }

            $attributes[$key] = $file;
        }

        return $attributes;
    }
}
