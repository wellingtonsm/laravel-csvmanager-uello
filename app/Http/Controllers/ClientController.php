<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ClientService;

class ClientController extends Controller
{
    private $client_service;

    public function __construct(ClientService $client_service)
    {
        $this->client_service = $client_service;
    }

    public function index()
    {
        $clients = $this->client_service->getAllClients();

        if (request()->has('export')) {
            return $this->client_service->downloadClientsCSV();
        }

        return view('clients.index')->withClients($clients);
    }

    public function store(Request $request)
    {
        $this->client_service->importClientsByCSV($request->get('csv'));

        return response()->json(array());
    }

    public function destroy($id)
    {
        $this->client_service->destroyClient($id);

        return back();
    }
}
