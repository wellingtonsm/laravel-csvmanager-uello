<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Client;
use Carbon\Carbon;
use League\Csv\{Reader, Statement};

class ImportCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csv:import {path : Path of the csv file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CSV Import';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->argument('path');

        $csv = Reader::createFromPath($path, 'r');
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);

        $stmt = (new Statement())->offset(0);

        $records = $stmt->process($csv);

        $count = 0;
        $clients = array();

        foreach ($records as $key => $record) {
            $record['cpf'] = preg_replace("/[^0-9]/", "", $record['cpf']);
            $record['cep'] = preg_replace("/[^0-9]/", "", $record['cep']);

            $client = Client::whereCpf($record['cpf'])
                ->orWhere('email', $record['email'])
                ->first();

            if (!is_null($client)) {
                continue;
            }

            $client = new Client();

            $client->nome = $record['nome'];
            $client->email = $record['email'];
            $client->data_nascimento = Carbon::createFromFormat('d/m/Y', $record['datanasc'])->toDateString();
            $client->cpf = $record['cpf'];
            $client->cep = $record['cep'];

            $endereco = parse_address($record['endereco'], $record['cep']);

            $client->logradouro = $endereco['logradouro'];
            $client->numero = $endereco['numero'];
            $client->complemento = $endereco['complemento'];
            $client->bairro = $endereco['bairro'];
            $client->cidade = $endereco['cidade'];
            $client->estado = $endereco['estado'];
            $client->latitude = $endereco['latitude'];
            $client->longitude = $endereco['longitude'];

            $client->save();

            $count++;
        }

        $this->info($count == 1 ? 'Importação de ' . $count . ' cliente realizada' : 'Importação de ' . $count . ' clientes realizada');
    }
}
