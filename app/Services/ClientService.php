<?php
/**
 * Created by PhpStorm.
 * User: Well
 * Date: 24/07/2018
 * Time: 21:56
 */

namespace App\Services;

use App\Entities\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use League\Csv\Reader;
use League\Csv\Writer;

class ClientService
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getAllClients()
    {
        return $this->client->paginate(15);
    }

    public function importClientsByCSV($path)
    {
        $path = public_path(rm_asset($path));

        Artisan::call("csv:import", array(
            'path' => $path
        ));

        sleep(2);
    }

    public function downloadClientsCSV()
    {
        $clients = $this->client->orderBy('nome')->get();

        $file = new \SplTempFileObject();
        $file->setCsvControl(';');

        $csv = Writer::createFromFileObject($file);
        $csv->insertOne(array('nome', 'email', 'datanasc', 'cpf', 'endereco', 'cep'));

        $clients_csv = array();

        foreach ($clients as $client) {
            $csv->insertOne(array(
                'nome' => $client->nome,
                'email' => $client->email,
                'datanasc' => Carbon::parse($client->data_nascimento)->format('d/m/Y'),
                'cpf' => mask($client->cpf, '###.###.###-##'),
                'endereco' => "{$client->logradouro}, {$client->numero} - {$client->bairro} - {$client->cidade}",
                'cep' => $client->cep
            ));
        }

        $csv->output("clients.csv");
    }

    public function destroyClient($id)
    {
        return $this->client->destroy($id);
    }
}
