<?php

use Jarouche\ViaCEP\HelperViaCep;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;
use Http\Adapter\Guzzle6\Client;
use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Geocoder\StatefulGeocoder;

function rm_asset($path)
{
    return str_replace(asset('/'), '', $path);
}

function parse_address($address, $cep)
{
    $parse_inital = explode('-', $address);

    $endereco = $parse_inital[0];
    $bairro = $parse_inital[1];
    $cidade = $parse_inital[2];

    $endereco_cep = HelperViaCep::getBuscaViaCEP('JSON', $cep);

    $numero_complemento = last(preg_split('/^.*?(?=\d)/', $endereco));
    $logradouro = $endereco_cep['result']['logradouro'] ?? rtrim(trim(str_replace($numero_complemento, '', $endereco)), ',');

    $complemento = trim(last(explode(' ', $numero_complemento, 2)));
    $numero = trim(str_replace($complemento, '', $numero_complemento));

    $client = new Client();
    $provider = new GoogleMaps($client, null, 'AIzaSyB_nnS9TeOBTdbA1BF67_LpEkCt8Q5FoBo');
    $geocoder = new StatefulGeocoder($provider, 'pt-br');

    $result = $geocoder->geocodeQuery(GeocodeQuery::create("{$logradouro}, {$numero} - {$cidade} - {$cep}"));
    $lat_long = $result->first()->getCoordinates()->toArray();

    return array(
        'logradouro' => $logradouro,
        'numero' => $numero,
        'complemento' => $complemento,
        'bairro' => $bairro,
        'cidade' => $cidade,
        'estado' => $endereco_cep['result']['uf'],
        'latitude' => $lat_long[0],
        'longitude' => $lat_long[1]
    );
}

function mask($val, $mask)
{
    $maskared = '';
    $k = 0;
    for ($i = 0; $i <= strlen($mask) - 1; $i++) {
        if ($mask[$i] == '#') {
            if (isset($val[$k]))
                $maskared .= $val[$k++];
        } else {
            if (isset($mask[$i]))
                $maskared .= $mask[$i];
        }
    }
    return $maskared;
}