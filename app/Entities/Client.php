<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $timestamps = false;

    protected $casts = [
        'data_nascimento' => 'date'
    ];

    public function getEnderecoAttribute()
    {
        return "{$this->logradouro}, {$this->numero} {$this->complemento} - {$this->bairro} - {$this->cidade}/{$this->estado}";
    }
}
