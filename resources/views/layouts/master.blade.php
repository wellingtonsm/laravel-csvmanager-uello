<!doctype html>
<html lang="{{ app()->getLocale() }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', "Gerenciador de CSV's")</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <style>
            .btn {
                width: auto;
                height: auto;
                min-height: 40px;
                font-size: 14px;
                color: #FFF;
                padding: 5px 15px;
                border: 2px solid transparent;
                outline: 0;
                border-radius: 4px;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                -o-border-radius: 4px;
                transition: all .4s ease-in-out;
                -moz-transition: all .4s ease-in-out;
                -webkit-transition: all .4s ease-in-out;
                -o-transition: all .4s ease-in-out;
                background-color: #46C364;
            }

            .title h1 {
                margin-bottom: 10px;
                padding-right: 20px;
                font-weight: 700;
                font-size: 2.5em;
                color: #0091e5;
            }

            .btn,
            .btn-link,
            strong {
                font-weight: 700;
            }

            .btn.active,
            .btn.focus,
            .btn:active,
            .btn:focus,
            .btn:hover {
                color: #FFF;
                outline: 0;
                background-color: #3DA856;
                border-color: #3DA856;
            }

            .btn-line {
                color: #4A4A4A;
                background: 0 0;
                border-color: #4A4A4A;
            }

            .btn-line:active,
            .btn-line:focus,
            .btn-line:hover {
                background: #0091e5;
                border-color: #0091e5;
                color: #FFF;
            }

            .btn-link {
                background: transparent;
                font-size: 14px;
                line-height: 1em;
                color: #4A4A4A;
                padding: 0;
                margin: 0;
            }

            .btn-link:active,
            .btn-link:focus,
            .btn-link:hover {
                background: transparent;
                border-color: transparent;
                color: #000;
            }

            .btn-file {
                position: relative;
                overflow: hidden;
            }

            .btn-file input {
                position: absolute;
                top: 0;
                right: 0;
                min-width: 100%;
                min-height: 100%;
                font-size: 100px;
                text-align: right;
                filter: alpha(opacity=0);
                opacity: 0;
                outline: none;
                background: white;
                cursor: inherit;
                display: block;
            }

            table td[class*=col-],
            table th[class*=col-] {
                position: static;
                display: table-cell;
                float: none;
            }

            .tabela-padrao {
                background-color: #FFF;
                margin: 0 0 20px;
                width: 100%;
            }

            .tabela-padrao td,
            .tabela-padrao th,
            .tabela-padrao tr {
                padding: 10px 15px;
            }

            .tabela-padrao th {
                background-color: #0091e5;
                font-size: 16px;
                font-weight: 600;
                height: 35px;
            }

            .tabela-padrao tr {
                border: 1px solid #E5E5E5;
            }

            .tabela-padrao td {
                color: #4A4A4A;
                font-size: 14px;
                font-weight: 400;
            }

            .tabela-padrao th,
            .tabela-padrao th .btn-link {
                text-transform: none;
                color: #FFF;
            }

            .tabela-padrao tr td .btn-link {
                display: inline-block;
                padding: 5px 0;
                font-size: 16px;
                color: #0091e5;
                font-weight: 700;
            }

            .icon .fa {
                margin-right: 7px;
            }
        </style>

        @stack('head_scripts')
    </head>

    <body>
        @yield('content')

        <!-- JavaScript -->
        <script src="{{ asset('js/app.js') }}"></script>

        @stack('body_scripts')
    </body>

</html>