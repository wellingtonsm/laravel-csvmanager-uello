@extends('layouts.master') @section('content')
<div class="mt-5"></div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12 title">
            <h1>Clientes</h1>
        </div>
    </div>

    <div class="my-4"></div>

    <div class="row">
        <div class="col-12 d-flex justify-content-end">
            <div class="row">
                <div class="col-12 d-flex justify-content-end">
                    <button class="btn btn-file icon mr-2">
                        <input type="file" class="custom-file-input" id="csv">
                        <i class="fa fa-upload"></i>
                        Importar CSV
                    </button>
                    <a class="btn btn-line icon" href="{{ route('clients.index', ['export']) }}">
                        <i class="fa fa-download"></i>
                        Exportar CSV
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="fazendo-upload" style="display: none">
        <div class="col-12">
            <div class="mt-3"></div>

            <div class="alert alert-info">
                Fazendo o Upload do Arquivo
            </div>
        </div>
    </div>

    <div class="row" id="concluido-upload" style="display: none">
        <div class="col-12">
            <div class="mt-3"></div>

            <div class="alert alert-success">

            </div>
        </div>
    </div>

    <div class="mt-3"></div>

    <div class="row">
        <div class="col-12">
            <table class="table table-hover tabela-padrao">
                <thead>
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">Email</th>
                        <th scope="col">Data de Nascimento</th>
                        <th scope="col">CPF</th>
                        <th scope="col">Endereço</th>
                        <th scope="col">CEP</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($clients as $client)
                    <tr>
                        <td>{{ $client->nome }}</td>
                        <td>{{ $client->email }}</td>
                        <td>{{ $client->data_nascimento->format('d/m/Y') }}</td>
                        <td>{{ mask($client->cpf, '###.###.###-##') }}</td>
                        <td>{{ $client->endereco }}</td>
                        <td>{{ mask($client->cep, '#####-###') }}</td>
                        <td style="padding: 0">
                            <form action="{{ route('clients.destroy', $client->id) }}" method="POST" onsubmit="return confirm('Deseja remover este cliente?');">
                                @csrf @method('DELETE')

                                <button class="btn btn-link icon">
                                    <i class="fa fa-trash"></i> Excluir
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection @push('body_scripts')
<script>
    $(document).ready(function() {
        $('#csv').on('change', function() {
            let $formData = new FormData();

            $formData.append('csv', $(this).get(0).files[0]);
            $('#fazendo-upload').toggle();

            axios.post('/clients', $formData).then(res => res.data).then(res => {
                $('#fazendo-upload').toggle();

                let $length = res.length;
                let $count = 3;

                $('#concluido-upload').find('.alert').html('Importação realizada');
                $('#concluido-upload').toggle();


                setInterval(function() {
                    if ($count > 0) {
                        $('#concluido-upload').find('.alert').html('Importação realizada.<br>Redirecionando em ' + $count +
                            ' ...');
                    } else {
                        location.reload();
                    }

                    $count--;
                }, 1000);
            });
        });
    });
</script>
@endpush