#### Requisitos

* Git
* Composer
* NGinx ou Apache 2

````
mod_rewrite       = on;
allow_url_fopen   = on;
````

* MySQL >= 5.7
* PHP >= 7.1

````
OpenSSL PHP Extension
PDO PHP Extension
Mbstring PHP Extension
Tokenizer PHP Extension
XML PHP Extension
Ctype PHP Extension
JSON PHP Extension 
ZIP PHP Extension
````

#### Documentação

Sistema desenvolvido utilizando framework Laravel 5.6
Link da documentação: [https://laravel.com/docs/5.6](https://laravel.com/docs/5.6)
    
#### Pastas do sistema
    
Tem toda a estrutura documentada no link abaixo:    
https://laravel.com/docs/5.6/structure 

#### Instalação

1. Direcionar o arquivo de configuração do site (Apache, Nginx) a acessar a pasta public do projeto
2. Copiar o arquivo .env.example para .env (NÂO APAGAR)
3. Alterar os dados necessários no arquivo .env recém criado

````
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=exemplo-nome-banco
DB_USERNAME=exemplo-usuario
DB_PASSWORD=exemplo-senha
````

4.0 Realizar os comandos abaixo na raiz do projeto, respeitando a finalização do anterior

````
$ composer install
$ php artisan key:generate
$ php artisan migrate
````

4.1 Executar o comando abaixo considerando o arquivo dump.sql na pasta database/scripts ou importar o dump diretamente e ignorar o comando abaixo

````
$ php artisan db:seed --class=DatabaseRawSeeder
````

4.2 Prosseguir com os comandos

````
$ php artisan storage:link
$ chgrp -R www-data storage bootstrap/cache
$ chmod -R ug+rwx storage bootstrap/cache
````

#### Extras 

Para importar um CSV atraves de linha de comando execute o comando abaixo considerando o parametro sendo o caminho completo do arquivo do csv

````
php artisan csv:import "C:\Users\Well\Downloads\clientes.csv"
````

#### Pacotes Utilizados

````
{
    "geocoder-php/google-maps-provider", - Buscar Geolocalização a partir do Endereço
    "league/csv", - Importação e Exportação de CSV's
    "jarouche/viacep" - Busca de Dados via CEP
}
````